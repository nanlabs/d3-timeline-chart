import React, { Component } from 'react';
import logo from './logo.svg';
import TimelineChart from './TimelineChart';
import './App.css';

class App extends Component {

  constructor() {
    super();

    this.events = [
      {'timestamp': 1528330570373, 'label': 'This is the first message', 'color': '#ea2314'},
      {'timestamp': 1527130560373, 'label': 'This is another message', 'color': '#eaff11'},
      {'timestamp': 1527030540373, 'label': 'My last msg is here', 'color': '#ea2314'},
      {'timestamp': 1527230550373, 'label': 'Three kingdoms', 'color': '#ea2314'},
      {'timestamp': 1527530530373, 'label': 'Korean Empire', 'color': '#eaff11'},
      {'timestamp': 1528030520373, 'label': 'There is a problem with this device', 'color': '#55ff11'},
      {'timestamp': 1528010310373, 'label': 'There is a problem with this device', 'color': '#55ff11'}
    ];
  }

  render() {
    let from = new Date(2018, 4, 6);
    let to = new Date(2018, 5, 6);

    return (
      <div className="App">
        <br />
        <TimelineChart id='chart2' 
          items={this.events}
          from={from.getTime()}
          to={to.getTime()}
         />
         <br />
      </div>
    );
  }
}

export default App;
