import React, { Component } from 'react';
import * as d3 from 'd3';
import './Timeline.css';

class TimelineChart extends Component {

  constructor(props) {
    super(props);

    this.margins = [20, 15, 15, 20]; // top right bottom left
    this.width = 700 - this.margins[1] - this.margins[3];
    this.height = 200 - this.margins[0] - this.margins[2];
    this.mini = {
      height: 30
    };
    this.main = {
      height: this.height - this.mini.height - 50
    };
    this.time = {
      from: this.props.from,
      to: this.props.to
    };

    this.circles = {
      radius: 6
    };

    this.scales = {};
    this.scales.x = d3.scaleLinear()
      .domain([this.time.from, this.time.to])
      .range([0, this.width]);
    this.scales.x1 = d3.scaleLinear()
      .range([0, this.width]);
  
  }

  render() {
    return (
      <div id={this.props.id}></div>
    );
  }

  componentDidMount() {

    let events = this.props.items.filter((e) => {
      return (e.timestamp >= this.time.from && e.timestamp <= this.time.to)
    });

		let chart = d3.select('#' + this.props.id)
			.append('svg')
      .attr('width', this.width + this.margins[1] + this.margins[3])
      .attr('height', this.height + this.margins[0] + this.margins[2])
    
    
    // Place main container

    let main = chart.append('g')
      .attr('transform', 'translate(' + this.margins[3] + ',' + this.margins[0] + ')')
      .attr('width', this.width)
      .attr('height', this.main.height)
      .attr('class', 'main')
    
    main.append('rect')
      .attr('class', 'mini-background')
      .attr('y', 1)
      .attr('fill', '#efefef')
      .attr('width', this.width)
      .attr('height', this.main.height)
    
    main.append('rect')
      .attr('transform', 'translate(0,' + this.main.height/2 + ')')
      .attr('width', this.width)
      .attr('height', 1)
      .attr('fill', '#ccc')


    // Place mini container

    let mini = chart.append('g')
      .attr('transform', 'translate(' + this.margins[3] + ',' + (this.main.height + this.margins[0]*2) + ')')
      .attr('width', this.width)
      .attr('height', this.mini.height)
      .attr('class', 'mini')
    
    mini.append('rect')
      .attr('class', 'mini-background')
      .attr('y', 1)
      .attr('fill', '#efefef')
      .attr('width', this.width)
      .attr('height', this.mini.height)

    mini.append('rect')
      .attr('transform', 'translate(0,' + this.mini.height/2 + ')')
      .attr('width', this.width)
      .attr('height', 1)
      .attr('fill', '#ccc')
    
    mini.append('g')
      .attr('class', 'events')
      .attr('transform', 'translate(0,' + this.mini.height/2 + ')')
      .selectAll('.events')
      .data(events)
      .enter().append('circle')
        .attr('fill', (d) => { return d.color; } )
        .attr('cx', this.circles.radius/2)
        .attr('cy', this.circles.radius/2)
        .attr('r', this.circles.radius)
        .attr('transform', (d) => { return 'translate(' + this.scales.x(d.timestamp) + ',' + this.circles.radius/-2 + ')'; })
    
        
    // Init brush

    this.brush = d3.brushX()
        .extent([[0, 0], [this.width, this.mini.height]])
        .on('brush', () => {
          this.brushed(d3.event);
        });

    mini.append('g')
      .attr('class', 'x brush')
      .call(this.brush)
      .selectAll('rect')
      .attr('y', 1)
      .attr('height', this.mini.height - 1);
  }

  brushed(e) {
    if (!e || !e.selection) return; // Ignore empty selections.
    let selection = e.selection.map((v) => Math.ceil(this.scales.x.invert(v)))
    let min = selection[0];
    let max = selection[1];

    this.renderMain(min, max);
  }

  renderMain(min, max) {
    this.scales.x1.domain([min, max]);

    let events = this.props.items.filter((e) => {
      return (e.timestamp >= min && e.timestamp <= max)
    })

    let main = d3.select('.main')

    d3.selectAll('.main circle').remove()

    main.append('g')
      .attr('class', 'events')
      .attr('transform', 'translate(0,' + this.main.height/2 + ')')
      .selectAll('.events')
      .data(events)
      .enter().append('circle')
        .attr('fill', (d) => { return d.color; } )
        .attr('cx', this.circles.radius/2)
        .attr('cy', this.circles.radius/2)
        .attr('r', this.circles.radius)
        .attr('transform', (d) => { return 'translate(' + this.scales.x1(d.timestamp) + ',' + this.circles.radius/-2 + ')'; })
  }

}

export default TimelineChart;
